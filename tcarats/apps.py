from django.apps import AppConfig

class TcaratsConfig(AppConfig):
    name = 'tcarats'
    verbose_name = 'Два карата'
