define([
    'marionette'
], function (Marionette) {
    var app = {};

    var PledgorController = Marionette.Object.extend({
        index: function () {
            var rootView = this.getOption('rootView');
            rootView.indexRoute();
        },
        showItem: function(item) {
            var rootView = this.getOption('rootView');
            rootView.showItemRoute(item);
        },
        addItem: function () {
            var rootView = this.getOption('rootView');
            rootView.addItemRoute();
        }
    });

    app.PledgorRouter = Marionette.AppRouter.extend({
        initialize: function (options) {
            this.controller = new PledgorController(options)
        },
        appRoutes: {
            '': 'index',
            'add': 'addItem',
            ':id': 'showItem',
            '*actions': 'index'
        }

    });


    return app
});