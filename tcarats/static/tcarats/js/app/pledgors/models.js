define([
    'backbone'
], function (Backbone) {
    var app = {};

    app.PledgorModel = Backbone.Model.extend({
        urlRoot: '.',
        defaults: {
            name: null,
            phone: null,
            ID_serial: null,
            ID_number: null,
            ID_date: null,
            ID_state: null,
            birthdate: null,
            address: null,
            post_address: null,
            discont_card: null,
            inn: null
        }
    });

    app.PledgorContractModel = Backbone.Model.extend({
        defaults: {
            name: null,
            date_in: null,
            date_out: null
        }
    });

    return app;
});