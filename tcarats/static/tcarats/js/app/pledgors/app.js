require([
    'marionette',
    'backbone',
    'pledgors/views',
    'pledgors/collections',
    'pledgors/models',
    'pledgors/routers'
],
function(Marionette, Backbone, views, collections, models, routers) {
    var App = Marionette.Application.extend({
        region: {
            el: '#content',
            replaceElement: true
        },
        onBeforeStart: function(app, options) {
            $('nav li').removeClass('active');
            $('nav li[name="pledgors"]').addClass('active');
        },

        onStart: function(app, options) {
            var collection = new collections.PledgorCollection();
            var model = new models.PledgorModel();

            const rootView = new views.RootView({
                model: model,
                collection: collection
            });

            new routers.PledgorRouter({rootView: rootView});
            this.showView(rootView);
            Backbone.history.start();
        }
    });

    var app = new App();
    app.start();

});
