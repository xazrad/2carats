define([
    'backbone',
    'backbone.paginator',
    'pledgors/models'
], function (Backbone, PageableCollection, models) {
    var app = {};

    app.PledgorCollection = PageableCollection.extend({
        model: models.PledgorModel,
        url: '.',
        state: {
           firstPage: 0
        }
    });

    app.PledgorContractCollection = Backbone.Collection.extend({
        initialize: function (options) {
            this.pledgor = options.pledgor

        },
        model: models.PledgorContractModel,
        url: function () {
            return '' +this.pledgor+'?action=contracts'
        }
    });

    return app;
});
