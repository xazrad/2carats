define([
    'marionette'
], function (Marionette) {
    var app = {};

    var LabelView = Marionette.View.extend({
        tagName: 'h1',
        className: 'page-title txt-color-blueDark',
        template: '#label',
        modelEvents: {
            'sync': 'render'
        },
        templateContext: function () {
            return {
                'add': this.getOption('add')
            }
        }
    });
    var TicketControView = Marionette.View.extend({
        name: "q",
        initialize: function () {
            var self = this;
            if (this.collection) {
                this.collection.queryParams[this.name] = function () {
                    return self.query() || null;
                };
            }
        },
        ui: {
            searchInput: 'input[name="search"]',
            searchButton: 'button[name="search"]'
        },
        events: {
            'click @ui.searchButton': 'search'
        },
        search: function () {
            var data = {};
            var query = this.query();
            if (query) data[this.name] = query;
            this.collection.getFirstPage({data: data, reset: true, fetch: true});
        },
        query: function() {
            this.value = this.getUI('searchInput').val();
            return this.value;
        }
    });

    var PaginatorView = Marionette.View.extend({
        size: 15,
        template: _.noop,
        collectionEvents: {
            'reset': 'stateChange',
            'add': 'stateChange',
            'remove': 'stateChange'
        },
        events: {
            'click a': 'goPage'
        },
        goPage: function (e) {
            var ind =  +($(e.currentTarget).text()) - 1;
            this.collection.getPage(ind, {reset: true});
        },
        makePages: function ($ul, data) {
            //    {firstPage: 0, lastPage: 54, currentPage: 0, pageSize: 25, totalPages: 55, totalRecords: 1359}
            var delta = 5;
            var left = data.currentPage - delta;
            var right;

            if (left <= 0) {
                right = data.currentPage + delta *2;
            } else {
                right = data.currentPage + delta + 1;
            }
            var last = data.lastPage;
            var range = [];
            var rangeWithDots = [];
            var l;

            for (var i = 1; i <= data.lastPage; i++) {
                if (i == 1 || i == last || i >= left && i < right) {
                    range.push(i);
                }
            }
            _.each(range, function(i){
                if (l) {
                    if (i - l === 2) {
                        rangeWithDots.push(l + 1);
                    } else if (i - l !== 1) {
                        rangeWithDots.push('...');
                    }
                }
                rangeWithDots.push(i);
                l = i;
            });
            _.each(rangeWithDots, function (ind) {
                var $li = $(document.createElement("li"));
                var $a = $(document.createElement("a"));
                $a.attr('href', "javascript:void(0);");
                $a.text(ind);
                $li.append($a);
                if (ind == '...') {
                    $li.addClass('disabled');
                }
                if (ind == data.currentPage + 1) {
                    $li.addClass('active');
                }
                $ul.append($li);
            });

        },
        stateChange: function (data) {
            this.$el.empty();
            var $ul = $(document.createElement("ul"));

            this.makePages($ul, this.collection.state);

            $ul.addClass('pagination pagination-sm');
            this.$el.html($ul);
        }
    });
    var ItemView = Marionette.View.extend({
        tagName: 'tr',
        template: '#ticket-item'
    });

    var ItemEmptyView = Marionette.View.extend({
        tagName: 'tr',
        template: '#ticket-item-empty'
    });

    var TicketCollectionView = Marionette.CollectionView.extend({
        tagName: 'tbody',
        childView: ItemView,
        emptyView: ItemEmptyView
    });

    var TicketDataCollectionView = Marionette.View.extend({
        template: "#ticket-collection",
        className: 'col-sm-12',
        regions: {
            tbody: {
                el: 'tbody',
                replaceElement: true
            },
            paginator: 'div[name="paginator"]'
        },
        onRender: function () {
            this.showChildView('tbody', new TicketCollectionView({
                collection: this.collection
            }));
            this.showChildView('paginator', new PaginatorView({
                collection: this.collection
            }));

        }
    });

    app.RootView = Marionette.View.extend({
        el: '#content',
        template: _.noop,
        regions: {
            label: 'div[name="label"]',
            control: 'div[name="control"]',
            data: 'div[name="row-data"]'
        },
        showItemRoute: function (itemId) {
            this.emptyRegions();
            this.model.set({id: itemId});
            this.showChildView('label', new LabelView({
                model: this.model
            }));

        },
        addItemRoute: function () {
          this.emptyRegions();
          this.showChildView('label', new LabelView({
              add: true
          }));
          this.showChildView('control', new TicketControView({
                template: "#control-model-add"
          }));
        },
        indexRoute: function () {
            this.emptyRegions();
            this.showChildView('label', new LabelView());
            this.showChildView('control', new TicketControView({
                collection: this.collection,
                template: "#control-collection"
            }));
            this.showChildView('data', new TicketDataCollectionView({
                collection: this.collection
            }));
            this.collection.fetch();
        }
    });

    return app;
});