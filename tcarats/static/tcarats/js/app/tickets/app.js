require([
    'jquery',
    'marionette',
    'backbone',
    'tickets/views',
    'tickets/collections',
    'tickets/models',
    'tickets/routers'
],
function($, Marionette, Backbone, views, collections, models, routers) {
    var App = Marionette.Application.extend({
        region: {
            el: '#content',
            replaceElement: true
        },
        onBeforeStart: function(app, options) {
            $('nav li').removeClass('active');
            $('nav li[name="tickets"]').addClass('active');
        },

        onStart: function(app, options) {
            var collection = new collections.TicketCollection();
            var model = new models.TicketModel();

            const rootView = new views.RootView({
                model: model,
                collection: collection
            });

            new routers.TicketRouter({rootView: rootView});
            Backbone.history.start();
        }
    });

    var app = new App();
    app.start();

});
