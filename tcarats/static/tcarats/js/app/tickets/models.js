define([
    'backbone'
], function (Backbone) {
    var app = {};

    app.TicketModel = Backbone.Model.extend({
        urlRoot: '.',
        defaults: {
            pledgor__name: null,
            pledgor_id: null,
            contract__name: null,
            contract_id: null,
            serial: null,
            date_in: null,
            date_out: null,
            sum: null,
            erp_code: null
        }
    });

    return app;
});