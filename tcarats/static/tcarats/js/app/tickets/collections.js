define([
    'backbone',
    'backbone.paginator',
    'tickets/models'
], function (Backbone, PageableCollection, models) {
    var app = {};

    app.TicketCollection = PageableCollection.extend({
        model: models.TicketModel,
        url: '.',
        state: {
           firstPage: 0
        }
    });

    return app;
});
