/**
 * Created by radik on 21.07.17.
 */

require.config({
    baseUrl: "/static/tcarats/js/app",
    paths: {
		'jquery': '../libs/jquery-2.1.1.min',
		'jquery-ui': '../libs/jquery-ui-1.10.3.min',
		'app.config': '../app.config',
		'jquery.ui.touch-punch': '../plugin/jquery-touch/jquery.ui.touch-punch.min',
		'bootstrap': '../bootstrap/bootstrap.min',
		'SmartNotification': '../notification/SmartNotification.min',
		'smartwidgets': '../smartwidgets/jarvis.widget.min',
		'jquery.easy-pie-chart': '../plugin/easy-pie-chart/jquery.easy-pie-chart.min',
		'jquery.sparkline': '../plugin/sparkline/jquery.sparkline.min',
		'jquery.validate': '../plugin/jquery-validate/jquery.validate.min',
		'jquery.maskedinput': '../plugin/masked-input/jquery.maskedinput.min',
		'select2': '../plugin/select2/select2.min',
		// 'bootstrap-slider': '../plugin/bootstrap-slider/bootstrap-slider.min',
		'jquery.mb.browser': '../plugin/msie-fix/jquery.mb.browser.min',
		'fastclick': '../plugin/fastclick/fastclick.min',
		'app': '../app.min',
		// 'voicecommand': '../speech/voicecommand.min',
		// 'smart.chat.ui': '../smart-chat-ui/smart.chat.ui.min',
		// 'smart.chat.manager': '../smart-chat-ui/smart.chat.manager.min'
        backbone: '../plugin/backbone-min',
		'backbone.radio': '../plugin/backbone.radio.min',
        underscore: '../plugin/underscore-min',
        marionette: '../plugin/backbone.marionette.min',
        'backbone.paginator': '../plugin/backbone-paginator/backbone.paginator'
    },
    deps: ['start'],
	shim: {
        'underscore': {
            exports: '_'
        },
		'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette':{
            deps:['underscore', 'backbone', 'jquery'],
            exports: 'Marionette'
        },
       'backbone.paginator': {
            deps: ['underscore', 'backbone'],
            exports: 'PageableCollection'
        },
        'app.config': ['jquery'],
        'jquery.ui.touch-punch': ['jquery'],
        'bootstrap': ['jquery'],
        'SmartNotification': ['jquery'],
        'smartwidgets': ['jquery'],
        'jquery.sparkline': ['jquery'],
        'jquery.mb.browser': ['jquery'],
        'app': ['jquery'],
        // 'smart.chat.ui': ['jquery'],
        // 'smart.chat.manager': ['jquery'],
        'start': [
            'jquery',
            'jquery-ui',
            'app.config',
            'jquery.ui.touch-punch',
            'bootstrap',
            'SmartNotification',
            'smartwidgets',
            'jquery.easy-pie-chart',
            'jquery.sparkline',
            'jquery.validate',
            'jquery.maskedinput',
            'select2',
            // 'bootstrap-slider',
            'jquery.mb.browser',
            'fastclick',
            'app',
            // 'voicecommand',
            // 'smart.chat.ui',
            // 'smart.chat.manager'
        ]
    }
});
