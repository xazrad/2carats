import os

from dbfread import DBF
from unipath import FSPath

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand

from tcarats import models

class Command(BaseCommand):
    help = 'Чтение dbf файлов и запись в БД'

    def handle(self, *args, **options):
        from data import dbf_models
        list_dbf = [
            # 'SC324.DBF',  # Залогодатели
            # 'SC66.DBF'  # Договоры залогодателей
            # 'SC102.DBF'  # Залоговые билеты
            # 'SC510.DBF'  # Кассы
            'DH210.DBF'  # Операция по залогу
            # 'RG507.DBF'  # Регистр касса
        ]
        data_folder = FSPath(settings.BASE_DIR, 'data')
        count = 0
        for dbf_name in list_dbf:
            table = DBF(FSPath(data_folder, dbf_name), encoding='cp1251')
            model = dbf_models.deserializer[dbf_name]
            for record in table.records:
                obj, created = model.parse(record)
                print(obj)
                count += 1
        print('LOADED {}'.format(count))


