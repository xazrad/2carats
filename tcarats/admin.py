from django.contrib import admin

from . import models


class ContractPledgorAdmin(admin.TabularInline):
    model = models.ContractPledgor
    show_change_link = True
    extra = 0
    readonly_fields = ('name', 'date_in', 'date_out')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.Pledgor)
class PledgorAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'birthdate', 'phone')
    inlines = [ContractPledgorAdmin]
