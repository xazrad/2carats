# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-29 16:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tcarats', '0014_auto_20171029_1647'),
    ]

    operations = [
        migrations.AddField(
            model_name='operationticket',
            name='type',
            field=models.CharField(max_length=9, null=True, verbose_name='Тип операции'),
        ),
    ]
