# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-28 17:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tcarats', '0012_auto_20171028_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='operationticket',
            name='erp_id',
            field=models.CharField(db_index=True, editable=False, max_length=9, null=True),
        ),
    ]
