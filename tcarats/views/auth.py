from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import AuthenticationForm
from django.http.response import HttpResponseRedirect


class LoginForm(AuthenticationForm):
    error_messages = {
        'invalid_login': "Неверные данные",
        'inactive': "Пользователь неактивен",
    }

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)
        return self.cleaned_data


class LoginView(auth_views.LoginView):
    template_name = 'tcarats/login.html'
    redirect_authenticated_user = False
    form_class = LoginForm

    def form_valid(self, form):
        user = form.get_user()
        auth_login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())
