from tcarats import models
from tcarats.views.base import BaseAjaxView, BaseTemplateView


class PledgorView(BaseAjaxView):
    def _get_contracts_collection(self, pledgor_id):
        contracts_obj = models.ContractPledgor.objects.filter(pk=pledgor_id).values('name', 'date_in', 'date_out')
        return list(contracts_obj)

    def _get_pledgor(self, pledgor_id):
        try:
            obj = models.Pledgor.objects.filter(pk=pledgor_id).values('id', 'name', 'phone', 'address', 'ID_serial',
                                                              'ID_number', 'ID_date', 'ID_state', 'birthdate',
                                                              'address', 'post_address', 'discont_card',
                                                              'inn').get()
        except models.Pledgor.DoesNotExist:
            return {}
        return obj

    def get(self, request, pledgor_id):
        action = request.GET.get('action', None)
        data = {}
        if action is None:
            data = self._get_pledgor(pledgor_id)
        if action == 'contracts':
            data = self._get_contracts_collection(pledgor_id)
        return data


class PledgorListView(BaseTemplateView):
    template_name = 'tcarats/pledgors.html'

    def get_ajax_data(self, request, *args, **kwargs):
        page = int(self.request.GET.get('page', 0))
        per_page = int(self.request.GET.get('per_page', 25))
        q = self.request.GET.get('q', '').strip()
        start_ind = page * per_page
        end_ind = start_ind + per_page

        query = models.Pledgor.objects
        if q:
            query = query.filter(name__icontains=q)
        count = query.count()
        data = [{'total_entries': count}]
        query = query.values('id', 'name', 'phone', 'address', 'ID_serial',
                             'ID_number', 'ID_date', 'ID_state')[start_ind: end_ind]
        data.append(list(query))
        return data
