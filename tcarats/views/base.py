from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http.response import HttpResponseForbidden
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator

from tcarats.utils import json_serial


class BaseTemplateView(TemplateView):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if not request.is_ajax():
            return super().get(request, *args, **kwargs)
        resp_data = self.get_ajax_data(request, *args, **kwargs)
        return JsonResponse(resp_data, safe=False, json_dumps_params={'default': json_serial})



class BaseAjaxView(View):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseForbidden()
        resp_data = super().dispatch(request, *args, **kwargs)
        return JsonResponse(resp_data, safe=False, json_dumps_params={'default': json_serial})
