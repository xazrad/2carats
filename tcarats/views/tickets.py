from tcarats import models
from tcarats.views.base import BaseAjaxView, BaseTemplateView


class TicketsListView(BaseTemplateView):
    template_name = 'tcarats/tickets.html'

    def get_ajax_data(self, request, *args, **kwargs):
        page = int(self.request.GET.get('page', 0))
        per_page = int(self.request.GET.get('per_page', 25))
        q = self.request.GET.get('q', '').strip()
        start_ind = page * per_page
        end_ind = start_ind + per_page

        query = models.TicketPledgors.objects
        if q:
            query = query.filter(erp_code__icontains=q)
        query = query.select_related('pledgor', 'contract')
        count = query.count()
        data = [{'total_entries': count}]
        query = query.values('id', 'serial', 'date_in', 'date_out', 'sum', 'erp_code',
                             'pledgor_id', 'pledgor__name', 'contract__name', 'contract_id')[start_ind: end_ind]
        data.append(list(query))
        return data
