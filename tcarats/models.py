from django.db import models


class Pledgor(models.Model):
    name = models.CharField('ФИО', max_length=100)
    address = models.CharField('Адрес', max_length=85)
    post_address = models.CharField('Почтовый адрес', max_length=85)
    phone = models.CharField('Телефоны', max_length=50)
    inn = models.CharField('ИНН', max_length=12, null=True)
    ID_serial = models.CharField('Паспорт серия', max_length=14)
    ID_number = models.CharField('Паспорт номер', max_length=14)
    ID_state = models.CharField('Паспорт, кем выдан', max_length=60)
    ID_date = models.DateField('Паспорт дата выдачи', null=True, blank=False)
    birthdate = models.DateField('Дата рождения', null=True, blank=False)
    discont_card = models.CharField('Дисконтная карта', max_length=6, null=True, blank=False)
    erp_id = models.CharField(max_length=9, null=True, db_index=True, editable=False)
    erp_code = models.CharField(max_length=8, null=True, db_index=True, editable=False)

    class Meta:
        verbose_name = 'Залогодатель'
        verbose_name_plural = 'Залогодатели'
        unique_together = ('erp_id', 'erp_code')
        ordering = ['name']

    def __str__(self):
        return '{}'.format(self.name)


class ContractPledgor(models.Model):
    name = models.CharField('Наименование', max_length=85)
    pledgor = models.ForeignKey(Pledgor, verbose_name='Залогодатель')
    erp_id = models.CharField(max_length=9, null=True, db_index=True, editable=False)
    erp_code = models.CharField(max_length=6, null=True, db_index=True, editable=False)
    date_in = models.DateField('Дата возникновения')
    date_out = models.DateField('Дата погашения', null=True)

    class Meta:
        verbose_name = 'Договор'
        verbose_name_plural = 'Договоры'
        unique_together = ('erp_id', 'erp_code')

    def __str__(self):
        return '{}'.format(self.name)


class TicketPledgors(models.Model):
    pledgor = models.ForeignKey(Pledgor, verbose_name='Залогодатель', null=True)
    contract = models.ForeignKey(ContractPledgor, verbose_name='Договор', null=True)
    serial = models.CharField('Серия ЗБ', max_length=4)
    date_in = models.DateField('Дата залога', null=True)
    date_out = models.DateField('Дата выкупа', null=True)
    sum = models.DecimalField('Cумма ссуды', max_digits=11, decimal_places=2, default=0)
    erp_id = models.CharField(max_length=9, null=True, db_index=True, editable=False)
    erp_code = models.CharField(max_length=8, null=True, db_index=True, editable=False)

    class Meta:
        verbose_name = 'Залоговый билет'
        verbose_name_plural = 'Залоговые билеты'
        unique_together = ('erp_id', 'erp_code')
        ordering = ['-date_in']


class OperationTicket(models.Model):
    ''' временная для тестирования, нефакт что нужна будет в дальнейшем
    '''
    ticket = models.ForeignKey(TicketPledgors, verbose_name='Залоговый билет', null=True)
    pledgor = models.ForeignKey(Pledgor, verbose_name='Залогодатель', null=True)
    period = models.IntegerField('Срок залога', default=0)
    type = models.CharField('Тип операции', max_length=9, null=True)
    erp_id = models.CharField(max_length=9, null=True, db_index=True, editable=False)

    class Meta:
        verbose_name = 'Операция по залоговому билету'
        verbose_name_plural = 'Операции по залоговым билетам'


class CashBox(models.Model):
    name = models.CharField('Наименование', max_length=25)
    erp_id = models.CharField(max_length=9, null=True, db_index=True, editable=False)
    erp_code = models.CharField(max_length=5, null=True, db_index=True, editable=False)

    class Meta:
        verbose_name = 'Касса'


class RegisterCash(models.Model):
    period = models.DateField()
    cashbox = models.ForeignKey(CashBox, null=True)
    sum = models.DecimalField('Cумма ссуды', max_digits=11, decimal_places=2, default=0)

    class Meta:
        verbose_name = 'Регистр касса (остаток)'