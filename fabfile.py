# _*_ coding: utf-8 _*_

import os

from fabric.api import cd, env, local, put, run
from fabric.colors import green
from fabvenv import virtualenv


env.use_ssh_config = True

env.colorize_errors = True
env.hosts = ['chicago2']

def test():
    env.hosts = ['chicago2']

# def prod():
#     env.hosts = ['rushmore-web']


BASE_DIR = '/opt/2carats'


def _check_dir(directoty=False):
    the_dir = BASE_DIR
    if directoty:
        the_dir = os.path.join(BASE_DIR, directoty)

    result = run('if test -d %s; then echo "exists"; fi' % the_dir)

    if result != 'exists':
        run('mkdir %s' % the_dir)


def prepare_deploy():
    _check_dir()
    _check_dir('log')
    _check_dir('private')
    _check_dir('static')
    _check_dir('media')

    with cd(BASE_DIR):
        run('chown www-data:www-data private')
        run('chown www-data:www-data log')
        run('chown www-data:www-data static')
        run('chown www-data:www-data media')
        run('python3 -m venv env')
        run('git clone git@bitbucket.org:xazrad/2carats.git project')

    nginx_conf = os.path.join(BASE_DIR, 'project', 'etc', 'nginx.production.conf')
    uwsgi_conf = os.path.join(BASE_DIR, 'project', 'etc', 'uwsgi.production.ini')

    run('ln -s %s /etc/nginx/sites-available/2carats.conf' % nginx_conf)
    run('ln -s /etc/nginx/sites-available/2carats.conf /etc/nginx/sites-enabled/2carats.conf')

    run('ln -s %s /etc/uwsgi/apps-available/2carats.ini' % uwsgi_conf)
    run('ln -s /etc/uwsgi/apps-available/2carats.ini /etc/uwsgi/apps-enabled/2carats.ini')

    run('/etc/init.d/uwsgi restart')
    run('/etc/init.d/nginx restart')


def update_code(branch='master'):
    with cd(os.path.join(BASE_DIR, 'project')):
        run('git fetch')
        run('git checkout %s' % branch)
        run('git pull origin %s' % branch)
        # run('source requirements/requirements.apt')

    with virtualenv(os.path.join(BASE_DIR, 'env')):
        run('pip install --upgrade pip')
        run('pip install -r %s' % (os.path.join(BASE_DIR, 'project', 'requirements.txt')))
        with cd(os.path.join(BASE_DIR, 'project')):
            run('python manage.py collectstatic --noinput')

    with cd(os.path.join(BASE_DIR, 'project')):
        run('touch touch-reload')

    # run('/etc/init.d/uwsgi restart')
    # run('supervisorctl restart all')


def transport_code():
    local('git status --porcelain > git_status.txt')

    with open('git_status.txt', 'r') as f:
        files = f.readlines()

    for file in files:
        mode, the_file = file.split()
        print(green(the_file))

        remotepath = os.path.join(BASE_DIR, 'project', the_file)

        put(the_file, remotepath)  # mirror_local_mode=True

    run('/etc/init.d/uwsgi restart')
    # run('supervisorctl restart all')


def checkout_all():
    with cd(os.path.join(BASE_DIR, 'project')):
        run('git stash save --keep-index')
        run('git stash drop')
