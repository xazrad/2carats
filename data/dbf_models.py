from tcarats import models

deserializer = {}


class Field():
    class arguments:
        db_field = ''
        fkey_model = ''

    def __init__(self, **kwargs):
        args = self.__class__.arguments
        for argnm in dir(args):
            if not argnm.startswith("__"):
                value = kwargs.get(argnm, getattr(args, argnm))
                setattr(self, argnm, value)

    def __get__(self, instance, owner=None):
        if instance is None:
            return self
        return instance.__dict__.get(self.field_name)

    def __set__(self, instance, value):
        instance.__dict__[self.field_name] = value

    def validate(self, value):
        return value


class ForeignField(Field):
    def validate(self, value):
        try:
            return self.fkey_model.objects.get(erp_id=value)
        except:
            return


class MetaModel(type):
    def __new__(cls, name, bases, attrs):
        cls = super(MetaModel, cls).__new__(cls, name, bases, attrs)
        parents = [b for b in bases if isinstance(b, MetaModel)]
        if not parents:
            return cls

        assert attrs.get('meta'), ('Defenition class model error'
                                   'class meta does not exist')

        # meta_attrs = {}
        # meta_attrs.update(attrs["meta"])
        # cls.meta = Meta(meta_attrs)
        cls_fields = []
        for (name, value) in attrs.items():
            if isinstance(value, Field):
                value.field_name = name
                cls_fields.append(value)
        cls._fields = cls_fields
        deserializer[cls.meta.dbf] = cls
        return cls


class BaseModelReg(metaclass=MetaModel):
    @classmethod
    def parse(cls, dbf_record):
        defaults = {}
        for field in cls._fields:
            field_name = field.field_name
            db_field = field.db_field
            value_prep = field.validate(dbf_record[field_name])
            defaults[db_field] = value_prep
        obj, created = cls.meta.model.objects.update_or_create(period=dbf_record['PERIOD'],
                                                               defaults=defaults)
        return obj, created

class BaseModelRef(metaclass=MetaModel):

    @classmethod
    def parse(cls, dbf_record):
        defaults = {}
        for field in cls._fields:
            field_name = field.field_name
            db_field = field.db_field
            value_prep = field.validate(dbf_record[field_name])
            defaults[db_field] = value_prep
        obj, created = cls.meta.model.objects.update_or_create(erp_id=dbf_record['ID'],
                                                               erp_code=dbf_record['CODE'],
                                                               defaults=defaults)
        return obj, created

class BaseModelDoc(metaclass=MetaModel):

    @classmethod
    def parse(cls, dbf_record):
        defaults = {}
        for field in cls._fields:
            field_name = field.field_name
            db_field = field.db_field
            value_prep = field.validate(dbf_record[field_name])
            defaults[db_field] = value_prep
        obj, created = cls.meta.model.objects.update_or_create(erp_id=dbf_record['IDDOC'],
                                                               defaults=defaults)
        return obj, created


class Pledgor(BaseModelRef):
    DESCR = Field(db_field='name')  # Наименование
    SP311 = Field(db_field='address')  # ЮридическийАдрес
    SP312 = Field(db_field='post_address')  # ПочтовыйАдрес
    SP313 = Field(db_field='phone')  # Телефоны
    SP314 = Field(db_field='inn')  # ИНН
    SP315 = Field(db_field='ID_serial')  # Документ серия
    SP316 = Field(db_field='ID_number')  # Документ номер
    SP317 = Field(db_field='ID_state')  # Документ кем выдан
    SP318 = Field(db_field='ID_date')  # Документ дата выдачии
    SP322 = Field(db_field='birthdate')  # Дата рождения
    SP566 = Field(db_field='discont_card')  # Дисконтная карта
    # ID = Field(db_field='erp_id')  # UID 1с
    # CODE = Field(db_field='erp_code')  # Код 1с

    class meta:
        model = models.Pledgor
        dbf = 'SC324.DBF'


class ContractPledgor(BaseModelRef):
    DESCR = Field(db_field='name')  # Наименование
    PARENTEXT = ForeignField(db_field='pledgor', fkey_model=models.Pledgor)  # Залогодатель
    SP51 = Field(db_field='date_in')  # ДатаВозникновения
    SP52 = Field(db_field='date_out')  # ДатаПогашенияОбяз

    class meta:
        model = models.ContractPledgor
        dbf = 'SC66.DBF'


class TicketPledgor(BaseModelRef):
    SP89 = ForeignField(db_field='pledgor', fkey_model=models.Pledgor)  # Залогодатель
    SP90 = ForeignField(db_field='contract', fkey_model=models.ContractPledgor)  # Договор
    SP100 = Field(db_field='serial')  # Серия ЗБ
    SP616 = Field(db_field='date_in')  # Дата залога
    SP617 = Field(db_field='date_out')  # Дата выкупа
    SP618 = Field(db_field='sum')  # сумма ссуды

    class meta:
        model = models.TicketPledgors
        dbf = 'SC102.DBF'


class OperationTicket(BaseModelDoc):
    SP182 = ForeignField(db_field='ticket', fkey_model=models.TicketPledgors)  # Залоговый билет
    SP184 = ForeignField(db_field='pledgor', fkey_model=models.Pledgor)  # Залогодатель
    SP186 = Field(db_field='period') # Срок залога
    SP498 = Field(db_field='type') # Срок залога

    class meta:
        model = models.OperationTicket
        dbf = 'DH210.DBF'


class CashBox(BaseModelRef):
    DESCR = Field(db_field='name')  # Наименование

    class meta:
        model = models.CashBox
        dbf = 'SC510.DBF'


class RegisterCashBox(BaseModelReg):
    SP508 = ForeignField(db_field='cashbox', fkey_model=models. CashBox)  # Касса
    SP509 = Field(db_field='sum')  # Сумма

    class meta:
        model = models.RegisterCash
        dbf = 'RG507.DBF'
